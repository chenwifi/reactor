#ifndef ACCEPTOR_H
#define ACCEPTOR_H

typedef struct acceptor acceptor_t;

acceptor_t *create_acceptor(int listenfd,reactor_t *);
static void *handle_event(void *msg);

struct acceptor{
	int listenfd;
	void *(*handle_event)(void *msg);
};

#endif
