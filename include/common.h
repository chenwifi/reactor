#ifndef COMMON_H
#define COMMON_H

#define USER_ERR_BUF 512
#define ERR_BUF 512
#define ERR_TEXT_BUF 1024 

void outputError(int err,const char *format, ...);

#endif
