#ifndef EVENT_H
#define EVENT_H

typedef struct event_handler event_handler_t;
typedef struct handle_event_msg handle_event_msg_t;
typedef struct reactor reactor_t;

struct event_handler{
	int fd;
	void *(*handle_event)(void *msg);
	reactor_t *reactor;
};

struct handle_event_msg{
	event_handler_t *eh;
	uint32_t events;
	int fd;
};

#endif
