#ifndef REACTOR_H
#define REACTOR_H

#define MAX_USERS 100

typedef struct reactor reactor_t;
typedef struct reactor_core reactor_core_t;
typedef struct event_handler event_handler_t;
typedef struct handle_event_msg handle_event_msg_t;
typedef struct _thpool_t thpool_t;
typedef struct acceptor acceptor_t;
typedef struct event_handler_list event_handler_list_t;
typedef struct event_handler_node event_handler_node_t;

reactor_t *create_reactor(int epollfd,thpool_t *thpool);
int add_event_handle(struct reactor *self,event_handler_t *eh);
int rm_event_handle(struct reactor *self,int fd);
int event_loop(struct reactor *self);
event_handler_t *find_eh(struct reactor *self,int fd);

struct reactor {
	int (*add_eh)(struct reactor *self,event_handler_t *eh);
	int (*rm_eh)(struct reactor *self,int fd);
	int (*event_loop)(struct reactor *self);
	thpool_t *thpool;
	reactor_core_t *core;
	acceptor_t *acceptor;
};

struct reactor_core{
	int epoll_fd;
	event_handler_list_t *eh_list;
};

struct event_handler_list{
	int current_len;
	event_handler_node_t *head;
	event_handler_node_t *tail;
};

struct event_handler_node{
	event_handler_t *eh;
	event_handler_node_t *next;
};

#endif
